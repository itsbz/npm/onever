# onever :: One version tool

A simple tool for NodeJS projects to manage single version of all modules.

## Installation

```shell
yarn add itsbz-onever
```

or

```shell
npm install itsbz-onever
```

## Usage

```shell
itsbz-onever --help
itsbz-onever init
itsbz-onever get
itsbz-onever set 1.0.1
itsbz-onever inc patch
itsbz-onever inc minor
itsbz-onever inc major
itsbz-onever apply
```

## Output sample
![screen_console](https://gitlab.com/its.bz/npm/onever/-/raw/master/screen_console.png)

## Contributing

0. Create project directory: `mkdir itsbz-onever && cd $_`
1. Clone repo: `git clone https://gitlab.com/its.bz/npm/onever.git ./`
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Bugs
Submit bugs [here](https://gitlab.com/its.bz/npm/onever/-/issues)

## History
```
{{#each releases}}{{title}} @ {{niceDate}}{{#commits}}  
  - {{subject}}{{/commits}}

{{/each}}
```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

MIT
